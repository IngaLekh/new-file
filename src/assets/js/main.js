$(function() {
    var beerList = [ ];
    $.ajax({
        url: 'https://api.punkapi.com/v2/beers',
        success: successHandler
    });
    function successHandler(response) {
        beerList = response;
        outputBeerList(beerList);
    }
    function outputBeerList(beerlist) {
        $('.allBeer').remove();
        if (beerList.length) {
            appendBeerList(beerlist);
            showBeerDescription();
        }
    }
    function appendBeerList(beerlist) {
        $.each(beerlist, function() {
            $('.beer-list').append('<div class="row allBeer" id="' +
                this.id + '" data-toggle="modal" data-target="#myModal" ><div class="col-xs-3 "><div class="name">' +
                this.name + '</div></div><div class="col-xs-2"><div class="id">' +
                this.id + '</div></div><div class="col-xs-4"><div class="tag">' +
                this.tagline + '</div></div><div class="col-xs-3"><div class="abv">' +
                this.abv + '</div></div></div>');
        });
    }
    function showBeerDescription() {
        $('.allBeer').on('click', function() {
            var id = $(this).attr('id');
            var beer = beerList.find(function(beer) {
                return Number(id) === Number(beer.id);
            });
            $('.modal-body p').remove();
            $('.modal-body').append('<p>' + beer.description + '</p>');
        });
    }
    $('#search-name').keyup(function() {
        var beerName = $(this).val().toLowerCase();
        var filtered = beerList.filter(function(beer) {
            return beer.name.toLowerCase().indexOf(beerName) !== -1;
        });
        outputBeerList(filtered);
    });
    $('#sort-name').on('click', function() {
        var beers = beerList.sort(function(a, b) {
            return a.name.localeCompare(b.name);
        });
        outputBeerList(beers);
    });
    $('#sort-id').on('click', function() {
        var beers = beerList.sort(function(a, b) {
            return Number(a.id) - Number(b.id);
        });
        outputBeerList(beers);
    });
    $('#sort-tagline').on('click', function() {
        var beers = beerList.sort(function(a, b) {
            return a.tagline.localeCompare(b.tagline);
        });
        outputBeerList(beers);
    });
    $('#sort-abv').on('click', function() {
        var beers = beerList.sort(function(a, b) {
            return a.abv - b.abv;
        });
        outputBeerList(beers);
    });
});